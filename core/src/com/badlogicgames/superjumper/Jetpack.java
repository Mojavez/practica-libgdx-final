package com.badlogicgames.superjumper;

/**
 * Created by JULIO on 16/04/2017.
 */

public class Jetpack extends GameObject {
    public static float JETPACK_WIDTH = 0.3f;
    public static float JETPACK_HEIGHT = 0.3f;

    public Jetpack (float x, float y) {
        super(x, y, JETPACK_WIDTH, JETPACK_HEIGHT);
    }
}
